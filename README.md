# README #

### To Use ###

- create a mysql database, user and password
- run the sql to define the table


### Set up your python ####
Create a python virtual environment and install libraries

```
python -m venv ./venv
. ./venv/bin/activate
pip3 install -r requirements.txt
```

##To run ###

set environment variables something like these:

```
REPORTING_DB_HOST=localhost
REPORTING_DB_PORT=3306
REPORTING_DB_NAME=reportingdb
REPORTING_DB_USER=reportuser
REPORTING_DB_PASSWORD=reportpass
FINTECH_BASE_URL=https://fintech-test.onrivabusiness.com
FINTECH_API_KEY=PUT_VALID_API_KEY_HERE
```


### Run the script ###

- python3 ./get_fintech_transactions.py


