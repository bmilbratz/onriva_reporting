"""
This script
 - polls fintech rest endpoint for transaction data in json format,
 - then inserts each transaction into a relational database.

Known issues:

- I generated sql DDL from json using a tool. It determined columns based on a sample of data.
It's possible that "later data" would have "lengthier values" for some field. As of today, inserts of
"lengthier data" would fail

"""
import datetime
import logging
import os

import mysql.connector
import requests
from dateutil import parser

log_level = os.getenv("LOG_LEVEL")
if not log_level:
    log_level = "INFO"
logging.basicConfig(level=log_level)

TABLE_NAME = "fintech_transactions"
"""
Name of table
"""


class FieldInfo:
    """
    Class to track field metadata from mysql database
    """

    def __init__(self, name, data_type, char_max_length):
        self.data_type = data_type
        self.char_max_length = char_max_length
        self.name = name

    def __repr__(self):
        return f"name:{self.name} {self.data_type}  {self.char_max_length}"


def delete_record(curs, table_name, data, primary_key_col):
    """
    Deletes records by primary key
    :param curs:
    :param table_name:
    :param data:
    :param primary_key_col:
    :return:
    """
    prim_key_data = {primary_key_col: data.get(primary_key_col)}
    prim_key_column = list(prim_key_data.keys())[0]
    prim_key_val = list(prim_key_data.values())[0]

    query = f"DELETE FROM {table_name} WHERE {prim_key_column} = '{prim_key_val}'"
    curs.execute(query)


def get_column_names(curs):
    """
    Builds a list of column names
    :param curs:  cursor
    :return:
    """
    query = """
     SELECT column_name, data_type, character_maximum_length  FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE TABLE_NAME LIKE 'fintech_transactions'
    """

    field_list = []
    curs.execute(query)
    records = curs.fetchall()
    for row in records:
        field_list.append(FieldInfo(row[0], row[1], row[2]))
    return field_list

    # field_names = [i[0] for i in curs.description]
    # return field_names


def insert_dict(curs, table_name, data):
    """
    Inserts a transaction record
    :param curs: cursor
    :param table_name: table name
    :param data: in dictionary form
    :return:
    """
    fields = data.keys()
    values = list(data.values())
    placeholder = "%s"
    field_list = ",".join(fields)
    placeholder_list = ",".join([placeholder] * len(fields))
    query = "insert into %s(%s) values (%s)" % (table_name, field_list, placeholder_list)
    logging.debug(f"insert query is {query}")
    try:
        curs.execute(query, values)
        logging.info(f"record inserted: {data.get('txid')}")
    except Exception as e:
        logging.error(f"error inserting row. fields/values follow: {e}")
        for x, y in data.items():
            logging.error(f"{x} {y}")


def main():
    """
    Main method
    :return:
    """
    reporting_db_host = os.getenv("REPORTING_DB_HOST")
    reporting_db_user = os.getenv("REPORTING_DB_USER")
    reporting_db_password = os.getenv("REPORTING_DB_PASSWORD")
    reporting_db_name = os.getenv("REPORTING_DB_NAME")

    cnx = mysql.connector.connect(user=reporting_db_user,
                                  password=reporting_db_password,
                                  host=reporting_db_host,
                                  database=reporting_db_name)

    cursor = cnx.cursor(buffered=True)

    field_info_list = get_column_names(cursor)
    field_info_dict = {}
    for field_info in field_info_list:
        field_info_dict[field_info.name] = field_info

    fintech_base_url = os.getenv("FINTECH_BASE_URL")
    fintech_api_key = os.getenv("FINTECH_API_KEY")

    # Default start date to 30 days ago
    start_date = os.getenv("START_DATE")
    if not start_date:
        start_date = (datetime.datetime.now() - datetime.timedelta(days=30)).strftime("%Y%m%d")

    headers = {
        'accept': 'application/json',
        'x-api-key': fintech_api_key
    }

    payload = {'start': start_date}
    endpoint_url = f'{fintech_base_url}/api/report/mergedtransactionreport'
    r_obj = requests.get(f"{endpoint_url}", params=payload, headers=headers, verify=False)

    cur_items = r_obj.json()
    logging.info(f"got this many: {len(cur_items)}")
    for item in cur_items:
        # remove cc trans
        # not sure how to eal with ccTrans as it's a list

        if item.get("customerCcTrans"):
            del item["customerCcTrans"]
        db_row = {}
        for k, v in item.items():
            logging.debug(f"k:{k}  type: {type(v)} dict?:{type(v) is dict}")

            if type(v) is dict:
                for k2, v2 in v.items():
                    col_name = f"{k}_{k2}".lower()
                    if type(v2) is dict:
                        for k3, v3 in v2.items():
                            col_name = f"{k}_{k3}".lower()
                            db_row[col_name] = v3
                    else:
                        db_row[col_name] = v2
            else:
                col_name = k.lower()
                if type(v) is list:
                    vx = " ".join(sorted(v))
                else:
                    vx = v
                db_row[col_name] = vx

        # Delete and insert record
        db_row_final = {}
        for k, v in db_row.items():
            field_info = field_info_dict.get(k)
            if field_info:
                # Date
                if k.endswith("date"):
                    val_date = parser.parse(v)
                    db_row_final[k] = val_date.strftime('%Y-%m-%d %H:%M:%S')
                # varchar
                elif v and field_info.data_type == "varchar":
                    if len(v) > field_info.char_max_length:
                        logging.error(f"field {k} length exceeds {field_info.char_max_length} value: {v}")
                    db_row_final[k] = v[:field_info.char_max_length]
                else:
                    db_row_final[k] = v
            else:
                logging.error(f"Missing column {str(k)} {type(k)}")

        delete_record(cursor, TABLE_NAME, db_row_final, "txid")
        insert_dict(cursor, TABLE_NAME, db_row_final)

    cursor.close()
    cnx.commit()
    cnx.close()


if __name__ == "__main__":
    main()
